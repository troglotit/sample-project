import React from "react";
import "./App.css";

import { Widget } from "./components/Widget";
import { PaymentSelector } from "./components/PaymentSelector";
import { Payment } from "./components/Payment";
import { DurationSelector } from "./components/DurationSelector";
import { Duration } from "./components/Duration";
import { FinalSum } from "./components/FinalSum";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentChoices: [
        {
          provider: "cards"
        },
        {
          provider: "yandexmoney"
        },
        {
          provider: "paypal"
        },
        {
          provider: "webmoney"
        },
        {
          sms: true
        },
        {
          provider: "qiwi"
        },
        {
          gift: true
        }
      ],
      durationChoices: [
        {
          months: 24,
          rate: 120
        },
        {
          months: 12,
          rate: 125
        },
        {
          months: 6,
          rate: 130
        }
      ],
      subscriptionGift: false,
      showSubscription: true,
      continuousSubscription: false,
      askSubscription: true,
      paymentChosen: false,
      durationChosen: false,
      useSale: false
    };
  }

  choosePayment(choosed_id) {
    this.setState(state => ({
      ...state,
      paymentChoices: state.paymentChoices.map((choice, choice_id) => ({
        ...choice,
        choosed: choosed_id === choice_id
      }))
    }));
    this.setState(state => {
      const isGiftClicked = this.state.paymentChoices[choosed_id].gift === true;
      const { subscriptionGift } = state;
      return {
        ...state,
        showSubscription: !isGiftClicked,
        subscriptionGift: isGiftClicked ? false : subscriptionGift
      };
    });
    this.setState(state => {
      const paymentChoice = this.state.paymentChoices[choosed_id];
      const askSubscription =
        ["cards", "yandexmoney", "paypal"].includes(paymentChoice.provider) ||
        paymentChoice.sms;
      const { continuousSubscription } = state;
      return {
        ...state,
        askSubscription,
        continuousSubscription: !askSubscription
          ? false
          : continuousSubscription
      };
    });
    this.setState(state => ({ ...state, paymentChosen: true }));
  }

  chooseDuration(choosed_id) {
    this.setState(state => ({
      ...state,
      durationChoices: state.durationChoices.map((choice, duration_id) => ({
        ...choice,
        choosed: choosed_id === duration_id
      }))
    }));
    this.setState(state => ({ ...state, durationChosen: true }));
  }

  handleGiftCheckbox() {
    this.setState(state => {
      const { subscriptionGift, paymentChoices } = state;
      return {
        ...state,
        subscriptionGift: !subscriptionGift,
        paymentChoices: paymentChoices.map(choice => {
          const { choosed } = choice;
          return {
            ...choice,
            choosed:
              choice.gift === true
                ? !subscriptionGift ? false : choosed
                : choosed
          };
        })
      };
    });
  }

  handleSubscriptionCheckbox() {
    this.setState(state => ({
      ...state,
      continuousSubscription: !state.continuousSubscription
    }));
  }

  handleSaleCheckbox() {
    this.setState(state => ({
      ...state,
      useSale: !state.useSale
    }));
  }

  render() {
    const {
      paymentChoices,
      durationChoices,
      subscriptionGift,
      continuousSubscription,
      showSubscription,
      askSubscription,
      paymentChosen,
      durationChosen,
      useSale
    } = this.state;
    const chosenDuration = durationChoices.find(({ choosed }) => choosed);

    return (
      <div className="App">
        <header>
          <h1>Домашний магазин</h1>
          <h2>Оформление подписки</h2>
          <div>Спасибо, что решили стать учатником клуба</div>
        </header>
        <Widget>
          <PaymentSelector>
            {paymentChoices.map((choice, id) => (
              <Payment
                key={id}
                onClick={this.choosePayment.bind(this, id)}
                {...choice}
              />
            ))}
          </PaymentSelector>
          {showSubscription ? (
            <div>
              <input
                id="gift"
                name="gift"
                type="checkbox"
                checked={subscriptionGift}
                onChange={this.handleGiftCheckbox.bind(this)}
              />
              <label htmlFor="gift">Покупаю подписку в подарок</label>
            </div>
          ) : null}
          {paymentChosen ? (
            <DurationSelector>
              {durationChoices.map((choice, id) => (
                <Duration
                  key={id}
                  onClick={this.chooseDuration.bind(this, id)}
                  {...choice}
                />
              ))}
            </DurationSelector>
          ) : null}
          {askSubscription && paymentChosen ? (
            <div>
              <input
                id="subscription"
                name="subscription"
                type="checkbox"
                checked={continuousSubscription}
                onChange={this.handleSubscriptionCheckbox.bind(this)}
              />
              <label htmlFor="subscription">
                Продлевать подписку автоматически
              </label>
            </div>
          ) : null}
          {durationChosen ? (
            <div>
              <FinalSum
                months={chosenDuration.months}
                rate={chosenDuration.rate}
                continuousSubscription={continuousSubscription}
                useSale={useSale}
              />
              <div>
                <input
                  id="sale"
                  name="sale"
                  type="checkbox"
                  checked={useSale}
                  onChange={this.handleSaleCheckbox.bind(this)}
                />
                <label htmlFor="sale">
                  Добавить подписку на скидку 5% на весь ассортимент товара
                </label>
              </div>
            </div>
          ) : null}
          <button type="button" disabled={!durationChosen}>
            Оплатить
          </button>
        </Widget>
      </div>
    );
  }
}

export default App;

import React from "react";

export class Duration extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { months, rate, onClick, choosed } = this.props;
    const isChoosed = choosed === true;
    const isntChoosed = choosed === false;
    const sum = months * rate;
    const year = months / 12;
    return (
      <div
        className={
          "Duration" +
          (isChoosed ? " choosed" : isntChoosed ? " not-choosed" : "")
        }
        onClick={onClick}
      >
        {year === 1 ? (
          <div>{year} год</div>
        ) : year > 1 ? (
          <div>{year} года</div>
        ) : (
          <div>{months} месяцев</div>
        )}
        <div>{sum} руб.</div>
        <div>{rate} руб./ месяц</div>
      </div>
    );
  }
}
